<!DOCTYPE html>
<html>
<head>
	<title>Ex5</title>
	<style>
		table{
			border-collapse: collapse;
		}
	</style>
</head>
<body>
	<center>
	<table border="2px" >
		<?php
			echo "<h2>Chess Board</h2>";
			for($row=1;$row<=8;$row++){
				echo "<tr>";
				for($col=1;$col<=8;$col++){
					$total = $row + $col;
					if ($total%2==0) {
						echo "<td height=30px width=30px bgcolor=#FFFFFF></td>";
					}
					else{
						echo "<td height=30px width=30px bgcolor=#000000></td>";
					}
				}
				echo "</tr>";
			}
		?>
	</table>
	</center>
</body>
</html>